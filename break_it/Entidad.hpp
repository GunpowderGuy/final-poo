#include <string>
#include <vector>

using namespace std;

class objeto {
public:
    int x;
    int y;
    int  dirx;
    int  diry;
    int next_x;
    int next_y;
//	int velocidad;<
    std::string display;
    bool rompible;
    int rangoColision;
};

void init_objeto(objeto& este, std::string show = "<123456789abcd>", int rango = 9, int n = 1
        , bool rompible = false, int mix = 5, int miy = 5, int offsetx = 6, int offsety = 0) {
    for (size_t i = 0; i < n; i++) {
        este.x = mix + (i * offsetx);
        este.y = miy + (i * offsety);
        este.dirx = 1;
        este.diry = 1;
        este.display = show;
        este.rangoColision = rango;
        este.rompible = rompible;
        // este[i].velocidad = 0;
        este.next_x;
        este.next_y;
    }
};

void init_bloques(vector<unique_ptr<objeto>> este, std::string show = "<123456789abcd>", int rango = 9
        , bool rompible = false, int mix = 5, int miy = 5, int offsetx = 6, int offsety = 0) {

    int n = este.size();

    for (size_t i = 0; i < n; i++) {
        este[i]->x = mix + (i * offsetx);
        este[i]->y = miy + (i * offsety);
        este[i]->dirx = 1;
        este[i]->diry = 1;
        este[i]->display = show;
        este[i]->rangoColision = rango;
        este[i]->rompible = rompible;
        // este[i].velocidad = 0;
        este[i]->next_x;
        este[i]->next_y;
    }
};
