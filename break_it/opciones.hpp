#include <iostream>
#include <stdlib.h>

void resetScreen() {
    for (size_t i = 0; i < 200; i++) std::cout << std::endl;
}

int menu(int &nvidas, int &velocidad_jugador, bool &aceleracion)
{
    int respuesta, respuesta2 ; bool respuesta3 = false;

    std::cout << "ELIJA UN COMANDO PARA CONTINUAR :\n\n\n";
    std::cout << "1.-   INICIO        \n";
    std::cout << "2.-   INSTRUCCIONES \n";
    std::cout << "3.-   ELEGIR NIVEL  \n";
    std::cout << "4.-   ELEGIR DIFICULTAD  <- muy importante si tienes problemas con la jugabilidad \n";
    std::cout << "5.-   SALIR         \n\n";
    std::cout << "COMANDO : ";

    do { std::cin >> respuesta; } while (respuesta != 1 && respuesta != 2 && respuesta != 3 && respuesta != 4 && respuesta != 5);
    std::cout << "\n\n";

    switch (respuesta)
    {
        case 1:
            break;

        case 2:
            std::cout << "  EL MOVIMIENTO DE ESTE JUEGO ES HORIZONTAL \n USAS LAS TECLAS 'A' Y 'S' PARA DESPLAZARTE \n\n A = IZQUIERDA \n D = DERECHA\n \n\n";
            std::cout << " - EL BOJETIVO DEL JUEGO ES MOVER TU PERSONAJE QUE EN ES CASO \n ES LA BARRA HORIZONTAL\n HACIENDO QUE CHOQUE CON LA PELOTA HACIENDO HACI DESAPARECER LOS CUADRADOS\n QUE SE ENCUENTRAN ENCIMA DE TU PERSONAJE  .\n\n";
            std::cout << "EL NIVEL ES COMPLETADO CUANDO NO SE ENCUENTRAN MAS CUADRADOS EN LA PANTALLA \n";
            std::cin.ignore(); std::cin.get();
            for (size_t i = 0; i < 200; i++) std::cout << std::endl;
            std::cout << "presione una tecla para continuar"; std::cin.ignore(); std::cin.get();
            for (size_t i = 0; i < 200; i++) std::cout << std::endl;
            return menu(nvidas, velocidad_jugador, aceleracion);
            break;

        case 3:
            std::cout << "INGRESE EL NUMERO DEL NIVEL DESEADO (NOTA: LA DIFICULTAD DEPENDE DE QUE TAN MAYOR SEA EL NIVEL) :\n\n1 : BASICO\n2 : INTERMEDIO\n3 : AVANZADO\n4 : PLUS ULTRA\n";
            std::cout << "\nCOMANDO : ";
            std::cin.ignore();
            do { std::cin >> respuesta; } while (respuesta != 1 && respuesta != 2 && respuesta != 3 && respuesta != 4);
            return respuesta;
            break;

        case 4:
            std::cout << "ingrese el numero de vidas ";
            std::cin >> respuesta;
            nvidas = respuesta;
            std::cout << "ingrese la velocidad del jugador ";
            std::cin >> respuesta2;
            velocidad_jugador = respuesta2;
            std::cout << "activar aceleracion ? 1 = si , 0 = no ";
            std::cin >> respuesta3;
            aceleracion = respuesta3;
            resetScreen();
            return menu(nvidas, velocidad_jugador, aceleracion);
            break;

        case 5:
            exit(EXIT_FAILURE);
            break;

        default:
            break;
    }

    return 1;
}

void fin(int score ) {
    // printf("\033c");
    // 	clear();

    std::string mensaje = "fin del juego";

    if (score > 10 ){
        mensaje = "GANADOR";
    }


    for (size_t i = 0; i < 200; i++) std::cout << std::endl;
    std::cout << mensaje;
    std::cin.ignore();std::cin.get();
    resetScreen();
}

