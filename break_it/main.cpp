#include <memory>
#include <chrono>
#include <thread>
#include "opciones.hpp"
#include "Entidad.hpp"
#include "cursesInit.hpp"

using namespace std;

int mover_pelota(objeto &pelota, int max_x, int max_y, int vidas = 0, float velocidad = 1) {
    pelota.next_x = pelota.x + (pelota.dirx * velocidad);
    pelota.next_y = pelota.y + (pelota.diry * velocidad);

    if (pelota.next_x >= max_x || pelota.next_x < 0)
        pelota.dirx = pelota.dirx * -1; else pelota.x += pelota.dirx * velocidad;

    if (pelota.next_y >= max_y || pelota.next_y < 0)
        pelota.diry = pelota.diry * -1; else pelota.y += pelota.diry * velocidad;

    if (pelota.next_y >= max_y) { vidas -= 1; flash(); }
    if (vidas <= 1) pelota.y += 99;

    return vidas;
}

static int defecto = 1;

void colision(vector<unique_ptr<objeto>> &bloque, objeto &pelota, int& puntaje = defecto) {
    int n = bloque.size();
    for (size_t i = 0; i < n; i++)
        if ((std::abs(pelota.next_x - (bloque[i]->x + bloque[i]->rangoColision)) <= bloque[i]->rangoColision)
        && pelota.next_y == bloque[i]->y) {
            pelota.diry = pelota.diry * -1;
            if (bloque[i]->rompible) {
                bloque[i]->y += 99; beep(); puntaje = puntaje + 1;
            }
        }
}

void movimientoJugador(objeto &playerA, char tecla, int velocidad = 6, int maxy = 180, int maxx = x) {
    objeto *player = &playerA;
    switch (tecla) {
        // case 'w':
        case '8':
            player->y -= velocidad;
            break;
            // 	case 's':
        case '2':
            player->y += velocidad;
            break;
        case 'a':
        case '4':
            if ( (player -> x - velocidad )  - player -> rangoColision >= 0)	player->x -= velocidad;
            break;
        case 'd':
        case '6':
            if ((player->x + velocidad + player -> rangoColision ) <= maxx )	player->x += velocidad;
            break;
        default:
            break;
    }
}

int main() {
    while (1) {
        auto aceleracion = std::make_unique<bool>();
        auto velocidad_jugador = std::make_unique<int>(6);
        auto vidas = std::make_unique<int>(9);
        menu(*vidas, *velocidad_jugador, *aceleracion); // el elemento 1 es 0 en los arrays , el array de niveles es de 0 a 4
        auto wnd = iniciar();
        maxSize(wnd);

        //int numeroNiveles = 4; int nbloques = 10;
        vector<unique_ptr<objeto>> bloques;
        init_bloques(bloques, "aassdsds", 2, true, 8, 8);


        std::unique_ptr<objeto> pelota = std::make_unique<objeto>(); init_objeto(*pelota, "0", 1);
        auto avatar = std::make_unique<objeto>(); init_objeto(*avatar);

        auto puntaje = make_unique<int>();

        while (*vidas >= -50 && *puntaje < 10) {
            int max_x, max_y; getmaxyx(stdscr, max_y, max_x);
            max_x = x;
            avatar->y = max_y - 3;
            set_title(*puntaje, *vidas);
            clear();

            mvprintw(pelota->y, pelota->x, "0");
            mvprintw(avatar->y, avatar->x, "<<<<<<<<<<<<>>>>>>>>>>>>");

            attron(COLOR_PAIR(1));
            for (size_t i = 1; i < bloques.size(); i++) {
                mvprintw(bloques[i]->y, bloques[i]->x, "    ");
            }

            attroff(COLOR_PAIR(1));

            refresh();
            std::this_thread::sleep_for(std::chrono::microseconds(DELAY));

            colision(bloques, *pelota, *puntaje);

            int velocidad = 1;
           // if (*aceleracion == true ) {velocidad = (((*puntaje / 2 <= 0 * 1) + *puntaje / 2)) + (nivelActual / 3);}
            *vidas = mover_pelota(*pelota, max_x, max_y, *vidas,velocidad );

            char tecla = wgetch(wnd);
            colision(bloques ,*pelota, *puntaje);
            movimientoJugador(*avatar, tecla, *velocidad_jugador, max_y, x);

        }
        endwin();
        fin(puntaje);
    }
    return 0;
}
