#include <curses.h>
#include <stdio.h>

#include <memory>
#include <string>
#include <cstring>

const int DELAY = 30000;

const int x = 90;

WINDOW* iniciar() {
    auto wnd = initscr();
    cbreak();
    noecho();
    curs_set(FALSE);
    keypad(wnd, true); nodelay(wnd, true);

    start_color();
    init_pair(1, COLOR_RED, COLOR_BLUE);
    init_pair(2, COLOR_RED, COLOR_GREEN);
    init_pair(3, COLOR_RED, COLOR_RED);
    init_pair(4, COLOR_RED, COLOR_MAGENTA);

    return wnd;
}

void maxSize(WINDOW* _window) {
#ifdef __WIN32
    /* Resize the terminal to something larger than the physical screen */
	resize_term(2000, 2000);

	int _rows, _cols;
	/* Get the largest physical screen dimensions */
	getmaxyx(_window, _rows, _cols);

	/* Resize so it fits */
	resize_term(_rows - 1, x);

	/* Get the screen dimensions that fit */
	getmaxyx(_window, _rows, _cols);
#endif
}

void reSize(WINDOW *win, int lines = 300, int columns = 300) {
    wresize(win, lines, columns);
}

void set_title(int score = 9,  int vidas = 5 , int velocidad = 1) {
    int nivel =1;
    std::string buffer;
    buffer += " score " + std::to_string(score);
    buffer += " nivel " + std::to_string(nivel);
    if (vidas >= 0) buffer += " vidas " + std::to_string(vidas); else buffer += " vidas 0";

    char *test;

#ifdef __linux__
    test = strdup(buffer.c_str());
    printf("%c]0;%s%c", '\033', test, '\007');
#elif _WIN32
    test = _strdup(buffer.c_str());
	PDC_set_title(test);
#else
	test = _strdup(buffer.c_str());
	printf("%c]0;%s%c", '\033', test, '\007');
#endif
}
